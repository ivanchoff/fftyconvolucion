#!/usr/bin/python
import math
import cmath
import numpy
from PIL import Image
import sys

def transpuesta(matriz):
    rows = len(matriz)
    cols = len(matriz[0])
    return [[matriz[j][i] for j in xrange(rows)] for i in xrange(cols)]

def omega(p, q):
    return cmath.exp((2.0 * cmath.pi * 1j * q) / p)

def fft(signal):
    n = len(signal)

    if n == 1:
        return signal
    else:
        Feven = fft([signal[i] for i in xrange(0, n, 2)])
        Fodd = fft([signal[i] for i in xrange(1, n, 2)])
        
        combined = [0] * n
        for m in xrange(n/2):
            combined[m] = Feven[m] + omega(n, -m) * Fodd[m]
            combined[m + n/2] = Feven[m] - omega(n, -m) * Fodd[m]
            
        return combined

def ifft(signal):
    n = len(signal)
    if n == 1:
        return signal
    else:
        Feven = fft([signal[i] for i in xrange(0, n, 2)])
        Fodd = fft([signal[i] for i in xrange(1, n, 2)])
        
        combined = [0] * n
        for m in xrange(n/2):
            combined[m] = ((Feven[m] + omega(n, m) * Fodd[m])/n)
            combined[m + n/2] = (((Feven[m] - omega(n, m) * Fodd[m])/n))
            
        return combined


def loadZeros(matriz, b = 0):
    if b == 0:
        l = math.log(len(matriz),2)
        if int(l) != l:
            l = 1 <<int(math.ceil(l))
        l=int(l)
    else:
        l = b
    
    for i in range(len(matriz),l):
        matriz.append(0)
    return matriz

def toInt(M):
# saca la magnitud de los numeros complejos que contien el pixel
    for i in range(len(M)):
        for j in range(len(M[i])):
            M[i][j][0]= int(abs(M[i][j][0]))
            M[i][j][1]= int(abs(M[i][j][1]))
            M[i][j][2]= int(abs(M[i][j][2]))
    return M

def inversa(M):
    mifft=M
    for i in range(len(M)):
        for j in range(len(M[0])):
            M[i][j]=ifft(M[i][j])
    return mifft

def mult(M,F):
    res=M
    for i in range(len(M)):
        for j in range(len(M[i])):
            res[i][j][0]=M[i][j][0] * F[i][j]
            res[i][j][1]=M[i][j][1] * F[i][j]
            res[i][j][2]=M[i][j][2] * F[i][j]
    return res

def transformada(M,filtro=0):
    if filtro==0:
        #print "transformada M: ",M[0][0]
        for i in xrange(len(M)):
            for j in range(len(M[i])):
                M[i][j] = fft(M[i][j])
            # M[i] = fft(M[i])
    else:
        for i in range(len(M)):
            M[i] = fft(M[i])
    return M



def fillImageZeros(img):
    # completa con ceros el arreglo del pixel(R,G,B,0)
    fftImg = img
    for i in range(len(fftImg)):
        for j in range(len(fftImg[i])):
            fftImg[i][j] = list(fftImg[i][j])
            fftImg[i][j].append(0)
    return  fftImg

def quitZero(M):
    for i in range(len(M)):
        for j in range(len(M[0])):
            M[i][j] = M[i][j][:3]
    print "quit Zero",M[0][0]
    #print M
    return M


def llenarFiltro(F,w,h):
    M=numpy.zeros(shape=(h,w),dtype=numpy.int)
    #M=numpy.ones(shape=(h,w),dtype=numpy.int)
    M=M.tolist()
    a=0
    for i in range((w/2)-1,(w/2)+2):
        b=0;
        for j in range((h/2)-1,(h/2)+2):
            M[i][j]=F[a][b]
            #M[i][j]=0
            b=b+1
        a=a+1
    # M[w/2][h/2] = 1
    return M

def main(imagen):
    img = Image.open(imagen)
    pix = img.load()
    data = []
    for y in xrange(img.size[1]):
        data.append([pix[x, y] for x in xrange(img.size[0])])
    
    #print data[58][58]
    data =  fillImageZeros(data)
    #print data[83][112]
    tdata = transformada(data)
    #print data[83][112]
    tdata = transpuesta(tdata)
    tdata = transformada(tdata)
    #print data[83][112]
    filtro = [[-2,0,-2],[0,8,0],[-2,0,-2]]
    filtro = llenarFiltro(filtro,len(data),len(data[0]))
   
    tfiltro = transformada(filtro,1)
    tfiltro = transformada(transpuesta(tfiltro),1)
    tmult = mult(tdata,tfiltro)
    print tmult[83][112]
    res = inversa(tmult)
    res = transpuesta(res)
    res = inversa(res)
    
    res = toInt(res)
    
    res = quitZero(res)
    print res[83][112]
    #print res
    v =Image.fromarray(numpy.uint8(numpy.asarray(res)))
    v.save('fftout.jpg')
   
if __name__ == '__main__':
    main(sys.argv[1])
