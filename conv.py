#!/usr/bin/python
# para ejecutar ~# python conv.py "ruta/imagen.jpg"

from PIL import Image
import sys

def convolucion(imActual):
    w,h = imActual.size
    pixeles = imActual.load()
    imCon = Image.new("RGB", imActual.size)
    conPix = imCon.load()
    M = [[0,0,0], [0,1,0], [0,0,0]]
    M.reverse()
    print M
    if imActual.mode == 'RGB':
        for x in range(w):
            for y in range(h):
                #suma = 0
                sumaR = 0
                sumaG = 0
                sumaB = 0
                for i in range(3):
                    for j in range (3):
                        try:
                            if x < w or y < h:
                                #suma += int(max(pixeles[(x-1)+i,(y-1)+j]) * M[j][i])
                                sumaR += pixeles[(x-1)+i,(y-1)+j][0] * M[j][i]
                                sumaG += pixeles[(x-1)+i,(y-1)+j][1] * M[j][i]
                                sumaB += pixeles[(x-1)+i,(y-1)+j][2] * M[j][i]
                        except IndexError:
                           # suma += 0
                            sumaR += 0
                            sumaG += 0
                            sumaB += 0
                conPix[x,y] = (sumaR,sumaG,sumaB)
    imCon.save('out.jpg')
                

if __name__ == '__main__':
    img=Image.open(sys.argv[1])
    convolucion(img)
    
